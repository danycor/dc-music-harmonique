FROM registry.hub.docker.com/library/nginx:stable-alpine
COPY nginx.conf /etc/nginx/nginx.conf
COPY ./ /usr/share/nginx/html
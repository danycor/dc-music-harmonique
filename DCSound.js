class DCSound {
    constructor() {
        this.frequence = 443;
        this.volume = 1; // 0-1
        this.temps = 2; // 0s-Xs
        this.channels = 2;
        this.augmentation = {
            positionTempsFin: 0, // 0s-temps
        }
        this.diminution = {
            positionTempsDebut: 1, // 0s-temps
        }
        this.harmonique = [0,0,0,0,0,0] // 0-1
    }

    generateSound(context) {
        var duree = context.sampleRate * this.temps;
        var arrayBuffer = context.createBuffer(this.channels, duree, context.sampleRate);
        for (var channel = 0; channel < this.channels; channel++) {
            var nowBuffering = arrayBuffer.getChannelData(channel);
            for (var i = 0; i < duree; i++) {
                
                // sin
                var value = Math.sin(2 * i / context.sampleRate * Math.PI * this.frequence);
                //console.log(value)
                
                // harmonique -> sin -> value
                var harmonique = this.harmonique;
                var frequence = this.frequence;
                harmonique.forEach(function (val) {
                    return value += Math.sin(frequence * (harmonique.indexOf(val)+2) * 2 * i / context.sampleRate * Math.PI) * val
                });
                //console.log(value)
                
                // moyenne (/ (sommeHarmonique +1))
                var hVal = this.harmonique.reduce((acc, val) => acc+val)+1;
                value /= hVal ? hVal : 1;
                //console.log(value)
                
                // augementation
                var augm = 1 / (duree - this.augmentation.positionTempsFin) * i;
                value *= Math.max(augm,1)

                // diminution
                var dim = 1 - 1 / (duree-this.diminution.positionTempsDebut) * (i + this.diminution.positionTempsDebut - duree) - 1 + 1 / (duree-this.diminution.positionTempsDebut) * this.diminution.positionTempsDebut;
                value *= Math.min(dim, 1);

                // volume
                value *= this.volume;
                //console.log(value)
                nowBuffering[i] = value;  // -1 ; 1
            }
        }
        return arrayBuffer;
    }

    playSound(context, buffer) {
        var source = context.createBufferSource();
        source.buffer = buffer;
        source.connect(context.destination);
        source.start(0);
    }
}

export default DCSound;
class Save {
    constructor() {
        this.name = '';
        this.frequence = 443;
        this.harmoniques = [0,0,0,0,0,0,0,0,0,0];
    }

    static getSaves() {
        return JSON.parse(localStorage.getItem('saves') ? localStorage.getItem('saves') : '[]')
    }
    static updateSaves(saves) {
        localStorage.setItem('saves', JSON.stringify(saves));
    }
    static addNew(name, frequence, harmoniques) {
        var saves = this.getSaves();
        var save = new Save();
        save.name = name;
        save.frequence = frequence;
        save.harmoniques = harmoniques;
        saves.push(save);
        this.updateSaves(saves);
    }

    static removeSave(saveId) {
        var saves = this.getSaves();
        var newSaves = saves.filter(function(value, index, arr){
            return index != saveId;
        });
        this.updateSaves(newSaves);
    }
}

export default Save;
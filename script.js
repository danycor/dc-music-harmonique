import DCSound from './DCSound.js'
import Save from './Save.js'

window.onload = init;
var context;    // Audio context

var soundChart;
var saving;

// context.sampleRate // rate

function init() {
    if (!window.AudioContext) {
        if (!window.webkitAudioContext) {
            alert("Your browser does not support any AudioContext and cannot play back this audio.");
            return;
        }
        window.AudioContext = window.webkitAudioContext;
        
    }
    context = new AudioContext();
    var DCS = new DCSound();
    var buffer = DCS.generateSound(context);
    // init graph
    var ctx = document.getElementById('sound-graph').getContext('2d');
    soundChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: buffer.getChannelData(0).slice(1, context.sampleRate / DCS.frequence),
            datasets: [{
                label: 'W1',
                data: buffer.getChannelData(0).slice(1, context.sampleRate / DCS.frequence),
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 0.2)'
                ]
            }]
        },
        options: {
            legend: {
                display: false
            },
            scales: {
                xAxes: [{
                    ticks: {
                        callback: function(value, index, values) {
                            return '';
                        }
                    }
                }]
            }
        }
    });

    // Vue
    saving = new Vue({
        el: '#saving-component',
        data: {
            saves: [
                {name: 'Gamma', frequence: 443, harmonique: [50,25,12,6,3,1,1,0]},
                {name: 'beta', frequence: 886, harmonique: [50,25,12,6,3,1,1,0]},
                {name: 'Alpha', frequence: 1772, harmonique: [50,25,12,6,3,1,1,0]}
            ]
        },
        methods: {
            load: function(save) {
                document.getElementById('frequence').value = save.frequence;
                for (var i = 1; i <= 10; i++) {
                    document.getElementById('harmonique-'+i).value = save.harmoniques[i-1]*100;
                }
            },
            del: function(save) {
                var i = this.saves.indexOf(save);
                Save.removeSave(i);
                this.saves = Save.getSaves();
            }
        },
        created: function () {
            this.saves = Save.getSaves();
        }
    })
}

document.getElementById('save-button').onclick = function (){
    var freq = parseInt(document.getElementById('frequence').value);
    freq = freq && freq >= 20 && freq <= 2000 ? (freq) : 443;

    var harmoniques = [];

    for (var i = 1; i <= 10; i++) {
        var har = parseInt(document.getElementById('harmonique-'+i).value);
        har = har && har >= 0 && har <= 100 ? har / 100 : 0;
        harmoniques.push(har);
    }
    var name = document.getElementById('save-name').value
    if(name != '') {
        Save.addNew(name, freq, harmoniques);
        saving.saves = Save.getSaves();
    }
}

document.getElementById('play-button').onclick = function (){
    var c1 = document.getElementById('channel-1').checked;
    var c= c1 ? 1 : 2;

    var vol = document.getElementById('volume').value;
    vol = vol && vol <=100 && vol >=0 ? vol/100 : 1;

    var dure = document.getElementById('duree').value;
    dure = dure ? parseFloat(dure) : 1;

    var aug = document.getElementById('temps-augmentation').value;
    aug = aug && aug<dure && aug >=0 ? parseFloat(aug) : 0;

    var dim = document.getElementById('temps-diminution').value;
    dim = dim && dim<=dure && dim >=0 ? parseFloat(dim) : dure;

    var freq = parseInt(document.getElementById('frequence').value);
    freq = freq && freq >= 20 && freq <= 2000 ? (freq) : 443;

    var harmoniques = [];

    for (var i = 1; i <= 10; i++) {
        var har = parseInt(document.getElementById('harmonique-'+i).value);
        har = har && har >= 0 && har <= 100 ? har / 100 : 0;
        harmoniques.push(har);
    }

    var context = new AudioContext();
    var DCS = new DCSound();
    DCS.channels = c;
    DCS.temps = dure;
    DCS.frequence = freq;
    DCS.volume = vol;
    DCS.augmentation.positionTempsFin = aug;
    DCS.diminution.positionTempsDebut = dim;
    DCS.harmonique = harmoniques;
    console.log(DCS)
    var buffer = DCS.generateSound(context);
    soundChart.data.labels = buffer.getChannelData(0).slice(1, context.sampleRate / freq);
    soundChart.data.datasets[0].data = buffer.getChannelData(0).slice(1, context.sampleRate / freq);
    soundChart.update();

    DCS.playSound(context, buffer);
}
